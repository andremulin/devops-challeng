# README #

Esse é o repositorio para o desafio de Infra Engenharia - Devops

### Aqui você encontrará o codigo de uma aplicação Node.JS ###

* Aplicação Node.JS
* Codigo SQL para estrutura base da aplicação

### O desafio consiste em ###

* ##### Montar uma estrutura em container com:
    * 1 Container rodando a aplicação em Node.js - Recomendado Node 10
    * 1 Container rodando um banco de dados MySQL - Recomendado Mysql 5.7
    * 1 Container rodando um servidor proxy

### Como montar a estrutura
* Preparar o banco de dados para ser usado pela aplicação usando o script database_schema.sql
* Utilizar as environment variables do projeto: MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD e API_PORT
* Executar a aplicação na porta 8080
* Colocar o container de proxy como reverse proxy servindo a aplicação na porta 80 ou 443
    * Script ACME com Lets Encripty é um PLUS
* Criar um docker-compose ou objeto YAML para kubernetes (kubectl apply) - Precisa ser funcional em uma stack de kubernetes ou docker
### PLUS +
* Fornecer comandos para criação e inicialização do ambiente de forma automatizada na AWS, GCP, AZURE ou Openstack via Terraform ou Ansible
### PLUS ++
* Criar uma estrutura de monitoramento usando Prometheus, Zabbix e/ou APM
    * Se utilizar Prometheus ou Zabbix, precisamos da stack em container (containerized)

### Como validar ###

* Use as operações de Criar Anotação, Listar Anotações e Remover Anotação da API - No arquivo chamadas.txt
* Ligar e Desligar a stack por kubectl ou docker-compose

### Como finalizar o desafio ###

* Envie um PR para esse mesmo repositorio com o seu codigo contendo tudo que foi necessario para chegar ao final dos testes, ou seja, Dockerfile, docker-compose, deploy.yaml (k8s), Prometheus Export e etc
    * Você pode fazer um clone no github e fazer o commit, mas garanta que o seu gitlab está aberto para acesso ao codigo.
* Assim que fizer o PR ou clone e compartilhe por email com selecao@mobicare.com.br.