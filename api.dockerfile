FROM node:10-alpine

COPY package.json /var/www/
COPY server.js /var/www/
WORKDIR /var/www
RUN npm install
ENTRYPOINT ["npm", "start"]
EXPOSE 8080